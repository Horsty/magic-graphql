# image de base utilisé
FROM node:8.10

# je créé un dossier ou sera situé mon app
RUN mkdir -p /src/app

# je travail à partir de se dossier
WORKDIR /src/app

# je copie divers fichier (bonne pratique pour le package)
COPY package-lock.json /src/app/package-lock.json
COPY package.json /src/app/package.json

# j'installe toutes les dépendances dont j'ai besoin
RUN npm install

# je copie l'entiéreté de mon app
COPY . /src/app

# j'ouvre le port 5454 de mon docker à l'extérieur
EXPOSE 4000

# la commande qui s'éxécute automatiquement au lancement du docker
CMD [ "npm", "start" ]
