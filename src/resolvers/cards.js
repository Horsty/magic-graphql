const baseURL = `https://api.magicthegathering.io/v1/cards`

const resolvers = {
  Query: {
    cards: (parent, args) => {
        const { name } = args
      return fetch(`${baseURL}/${name}`).then(res => res.json())
    },
    card: (parent, args) => {
      const { id } = args
      return fetch(`${baseURL}/${id}`).then(res => res.json())
    }
    /*,
    posts: () => {
      return fetch(`${baseURL}/posts`).then(res => res.json())
    },
    post: (parent, args) => {
      const { id } = args
      return fetch(`${baseURL}/blog/posts/${id}`).then(res => res.json())
    },*/
  },
}