const { GraphQLServer } = require("graphql-yoga");
var fetch = require("node-fetch");

const baseURL = `https://api.magicthegathering.io/v1/cards`;

const resolvers = {
  Query: {
    cards: (parent, args) => {
      const { name } = args;
      return fetch(`${baseURL}?name=${name}`)
        .then(res => res.json())
        .then(res => res.cards)
        .catch(error => console.log(error));
    },
    card: (parent, args) => {
      const { id } = args;
      return fetch(`${baseURL}/${id}`).then(res => res.json());
    }
  }
};

console.log("graohqlconstruct");
const server = new GraphQLServer({
  typeDefs: "./src/schema/card.graphql",
  resolvers
});

console.log("afeter");

server.start(() => console.log(`Server is running on http://localhost:4000`));
